class Employee{
    constructor(name,age,salary){
        this._name=name;
        this._age=age;
        this._salary=salary;
    }

    get name(){
        return this._name;
    }
    set name(newName){
        this._name=newName;
    }


    get age(){
        return this._age;
    }
    set age(newAge){
        this._age=newAge;
    }


    get salary(){
        return this._salary;
    }
    set salary(newSalary){
        this._salary=newSalary;
    }
}


class Programmer extends Employee{
    constructor(_name,_age,_salary,lang){
        super(_name,_age,_salary);
        this.lang=lang;
    }
    get salary(){
        return this._salary*3;
    }
}

const Iryna=new Programmer("Iryna",20,20000,["english","ukrainian"]);
console.log(Iryna);
console.log(Iryna.salary);

const Andrey=new Programmer("Andrey",24,40000,["ukrainian"]);
console.log(Andrey);
console.log(Andrey.salary);

const Petro=new Programmer("Petro",23,10000,["ukrainian"]);
console.log(Petro);
console.log(Petro.salary);